const express = require('express');
const router = express.Router();
const users = require('./users');
const tasks = require('./tasks');
const projects = require('./projects');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/', users);
router.use('/', tasks);
router.use('/', projects);

module.exports = router;
