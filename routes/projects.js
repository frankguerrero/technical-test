var express = require('express');
var router = express.Router();
const projectController = require('../controller/projectController');

router.get('/projects/:userId', projectController.getAllProjects);
router.post('/project', projectController.createProject);

module.exports = router;
