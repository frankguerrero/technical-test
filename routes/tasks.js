var express = require('express');
var router = express.Router();
const taskController = require('../controller/taskController');

router.get('/tasks/:userId', taskController.getAllTasks);
router.post('/task-manager', taskController.taskManager);

module.exports = router;
