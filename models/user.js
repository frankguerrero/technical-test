const { insertUser, getUserDBById } = require('../services/dynamoDB');


const createUser = async (user) => {
    try {
        await insertUser(user);
        return user;
    } catch (error) {
        return error;
    }
}

const getUserById = async (userId) => {
    try {
        const user = await getUserDBById(userId);
        if (user.length > 0)
            return user[0];
        else throw 'USER_NOT_FOUND';
    } catch (error) {
        return error;
    }
}

module.exports = {
    createUser,
    getUser: getUserById
}