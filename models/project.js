const { insertProject, getProjectDBByName, getProjectsDB } = require('../services/dynamoDB');


const createProject = async (project) => {
    try {
        const projectDB = await getProjectDBByName(project);
        if (projectDB.length > 0) throw { code: 'PROJECT_ALREADY_EXISTS', message: 'This user already has a project with this name' };
        project.duration = 0;
        await insertProject(project);
        return project;
    } catch (error) {
        return error;
    }
}

const saveProject = async (project) => {
    try {
        await insertProject(project);
        return project;
    } catch (error) {
        return error;
    }
}

const getProjectByName = async (attributes) => {
    try {
        const project = await getProjectDBByName(attributes);
        if (project.length > 0)
            return project[0];
        else throw 'PROJECT_NOT_FOUND';
    } catch (error) {
        return error;
    }
}

const getProjects = async (userId) => {
    try {
        const projects = await getProjectsDB(userId);
        return projects;
    } catch (error) {
        return error;
    }
}

module.exports = {
    createProject,
    getProjectByName,
    saveProject,
    getProjects
}