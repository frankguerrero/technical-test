const { searchTaskByNameAndProjectName, insertTask, getTasksDB } = require('../services/dynamoDB');

const searchTask = async (attributes) => {
    try {
        const data = await searchTaskByNameAndProjectName(attributes)
        return data;
    } catch (error) {
        return error;
    }
}

const saveTask = async (task) => {
    try {
        await insertTask(task);
        return task;
    } catch (error) {
        return error;
    }
}

const getTasks = async (userId) => {
    try {
        const tasks = await getTasksDB(userId);
        return tasks;
    } catch (error) {
        return error;
    }
}

module.exports = {
    searchTask,
    saveTask,
    getTasks
}