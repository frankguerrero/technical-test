const taskStatus = {
    started: "STARTED",
    restarted: "RESTARTED",
    paused: "PAUSED",
    stopped: "STOPPED"
} 

module.exports = {
    taskStatus
}