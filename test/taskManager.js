const assert = require('assert');
const request = require('supertest');
const localRequest = request('http://localhost:3000')


describe('taskController', function () {
    describe('#task-manager', function () {
        it('validate if the user does not exists', async () => {
            try {
                await localRequest.post('/task-manager')
                    .set('Content-Type', 'application/json')
                    .set('userid', 'invalid')
                    .expect(400);
            } catch (error) {
                throw error;
            }
        });

        it('validate if it is posible create a task without body', async () => {
            try {
                await localRequest.post('/task-manager')
                    .set('Content-Type', 'application/json')
                    .set('userid', 'TC7ymw4aD')
                    .expect(200);
            } catch (error) {
                throw error;
            }

        });

        it('validate if it is posible create a task only with duration', async () => {
            try {
                await localRequest.post('/task-manager')
                    .set('Content-Type', 'application/json')
                    .set('userid', 'TC7ymw4aD')
                    .send({ duration: 5000 })
                    .expect(200)
            } catch (error) {
                throw error;

            }

        });

        it('validate if it is posible create a task with wrong parameters in body', async () => {
            try {
                await localRequest.post('/task-manager')
                    .set('Content-Type', 'application/json')
                    .set('userid', 'TC7ymw4aD')
                    .send({ name: 5000, projectName: 3, duration: 'frank', status: "status" })
                    .expect(400)
            } catch (error) {
                throw error;
            }
        });
    })
});
