const moment = require('moment');
const userModel = require('../models/user');
const taskModel = require('../models/task');
const projectModel = require('../models/project');
const shortid = require('shortid');
const { taskScheme } = require('../schemes/taskScheme');
const { taskStatus } = require('../const/taskStatus');

const getAllTasks = async (req, res) => {
    try {
        const { params: { userId } } = req;
        const user = await userModel.getUser(userId);
        if (user == 'USER_NOT_FOUND') throw { code: user, message: 'The user does not exists' };
        const tasks = await taskModel.getTasks(userId);
        const sortedTask = sortTask(tasks);
        return res.json(sortedTask)
    } catch (error) {
        res.status(400).json(error);
    }
};

const taskManager = async (req, res) => {
    try {
        await taskScheme.validateAsync(req.body);
        let { name, projectName, duration, status } = req.body;
        const { userid: userId } = req.headers;
        if (!userId) throw { code: "USERID_REQUIRED_IN_HEADER", message: 'Without userId is not possible to continue' };
        const fields = { name, projectName, duration, status, userId };
        adjustFields(fields);
        const attributes = JSON.parse(JSON.stringify(fields));
        console.log({ attributes });
        const user = await userModel.getUser(userId);
        if (user == 'USER_NOT_FOUND') throw { code: user, message: 'The user does not exists' };
        const task = await taskModel.searchTask(attributes);
        const processedTask = await handlerTask(task, attributes)
        console.log({ processedTask });
        if (processedTask instanceof Error) throw processedTask;
        return res.json(processedTask)
    } catch (error) {
        console.log({ error });

        res.status(400).jsonp(error)
    }
};

const adjustFields = (fields) => {
    cleanField(fields);
    fields.name = fields.name ? fields.name : ' ';
    if (fields.status === taskStatus.restarted) fields.status = taskStatus.started;
}

const cleanField = (fields) => {
    for (const key in fields) {
        if (!['name', 'projectName', 'duration', 'status', 'userId'].includes(key)) {
            delete fields[key];
        }
    }
}

const handlerTask = async (task, attributes) => {
    try {
        const { projectName, duration, status, userId } = attributes
        let processedTask;
        let project;
        if (projectName) {
            const newProject = { name: projectName, userId, projectId: shortid.generate() }
            await projectModel.createProject(newProject);
            project = await projectModel.getProjectByName(newProject);
        }
        if (!task) {
            if (duration) {
                processedTask = await createTaskWithDuration(attributes);
                if (project) {
                    project.duration += duration;
                    await projectModel.saveProject(project);
                }
            } else {
                processedTask = await createStartedTask(attributes)
            }
        } else {
            if (!status) throw {
                code: 'INVALID_ACTION',
                message: 'If the task exists, status is required'
            };
            processedTask = await handlerTaskExists(status, task, project);
        }
        return processedTask;

    } catch (error) {
        return error;
    }
}

const createTaskWithDuration = async (attributes) => {
    const { name, projectName, duration, userId } = attributes
    const task = {
        name,
        projectName,
        duration,
        status: taskStatus.stopped,
        userId,
        createdAt: `${moment().format()}`,
        taskId: shortid.generate()
    }
    try {
        await taskModel.saveTask(task);
        return task;
    } catch (error) {
        return error;
    }
}

const createStartedTask = async (attributes) => {
    const { name, projectName, userId } = attributes
    const task = {
        name,
        projectName,
        duration: 0,
        status: taskStatus.started,
        userId,
        createdAt: `${moment().format()}`,
        taskId: shortid.generate(),
        startedAt: moment().format()
    }
    try {
        await taskModel.saveTask(task);
        return task;
    } catch (error) {
        return error;
    }
}

const handlerTaskExists = async (status, task, project) => {
    let processedTask;
    switch (status) {
        case taskStatus.started:
            if (task.status === taskStatus.started) {
                const duration = calculateDurationTask(task);
                await assignDurationAndStatus(project, task, duration, status);
            }
            processedTask = await startTask(task)
            break;
        case taskStatus.stopped: case taskStatus.paused:
            if (task.status === status || (status === taskStatus.paused && task.status === taskStatus.stopped))
                throw {
                    code: 'INVALID_ACTION',
                    message: `It is not possible ${status.toLowerCase()} a task if the task has status ${task.status}`
                };
            let duration = calculateDurationTask(task);
            if (status === taskStatus.stopped && task.status === taskStatus.paused) duration = 0;
            processedTask = await assignDurationAndStatus(project, task, duration, status);
            break;
    }
    return processedTask;
}

const startTask = async (task) => {
    try {
        task.status = taskStatus.started;
        task.startedAt = moment().format();
        await taskModel.saveTask(task);
        return task;
    } catch (error) {
        return error;
    }
}

const calculateDurationTask = (task) => {
    const startedAt = moment(task.startedAt);
    const now = moment();
    return now - startedAt;
}

const assignDurationAndStatus = async (project, task, duration, status) => {
    try {
        project.duration += duration;
        task.duration += duration;
        task.status = status;
        await projectModel.saveProject(project);
        await taskModel.saveTask(task);
        return task;
    } catch (error) {
        return error;
    }
}


const sortTask = (task) => {
    const sorted = task.sort(function (a, b) {
        if (a.createdAt > b.createdAt) {
            return -1;
        } else if (b.createdAt > a.createdAt) {
            return 1;
        } else {
            return 0
        }
    });

    return sorted;
}




module.exports = {
    getAllTasks,
    taskManager
}