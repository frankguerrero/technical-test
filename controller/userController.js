const shortid = require('shortid');
const userModel = require('../models/user');

const getUser = async(req, res, next) => {
    try {
        const { params: { userId } } = req;
        const user = await userModel.getUser(userId);
        return res.json(user)
    } catch (error) {
        return res.status(400).json(error)
    }
}

const createUser = async (req, res, next) => {
    try {
        const { body: { name } } = req;
        const user = {
            userId: shortid.generate(),
            name
        }
        await userModel.createUser(user)
        return res.json(user);
    } catch (error) {
        return next(error);
    }
}

module.exports = {
    getUser,
    createUser
}