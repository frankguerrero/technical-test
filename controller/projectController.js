const userModel = require('../models/user');
const projectModel = require('../models/project');
const shortid = require('shortid');

const getAllProjects = async (req, res) => {
    try {
        const { params: { userId } } = req;
        const user = await userModel.getUser(userId);
        if (user == 'USER_NOT_FOUND') throw { code: user, message: 'The user does not exists' };
        const projects = await projectModel.getProjects(userId);
        return res.json(projects)
    } catch (error) {
        res.status(400).json(error);
    }
}

const createProject = async (req, res) => {
    const { body: { name }, headers: { userid: userId } } = req;

    try {
        const user = await userModel.getUser(userId);
        if (user == 'USER_NOT_FOUND') throw { code: user, message: 'The user does not exists' };;
        if (!name) throw { code: 'NAME_IS_REQUIRED', message: 'The name is required to create a project' };
        const newProject = { name, userId, projectId: shortid.generate() }
        const project = await projectModel.createProject(newProject);
        return res.json(project);
    } catch (error) {
        return res.status(400).jsonp(error);
    }
}

module.exports = {
    getAllProjects,
    createProject
} 