const Joi = require('@hapi/joi');

const taskScheme = Joi.object({
    name: Joi.string().optional().allow(null),
    projectName: Joi.string().optional().allow(null),
    status: Joi.string().valid('STARTED', 'RESTARTED', 'PAUSED', 'STOPPED').optional().allow(null),
    duration: Joi.number().optional().allow(null),
})

module.exports = {
    taskScheme
}