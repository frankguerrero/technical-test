const AWS = require('aws-sdk');

AWS.config.update({
    region: 'us-east-1',
});

const userTable = 'fdg_users';
const projectTable = 'fdg_projects';
const taskTable = 'fdg_tasks';
const docClient = new AWS.DynamoDB.DocumentClient();


const insertUser = async (Item) => {
    try {
        const params = {
            TableName: userTable,
            Item,
        };
        const res = await docClient.put(params).promise();
        return res;
    } catch (error) {
        return error;
    }
}

const insertProject = async (Item) => {
    try {
        const params = {
            TableName: projectTable,
            Item,
        };
        const res = await docClient.put(params).promise();
        return res;
    } catch (error) {
        return error;
    }
}

const insertTask = async (Item) => {
    try {
        const params = {
            TableName: taskTable,
            Item,
        };
        const res = await docClient.put(params).promise();
        return res;
    } catch (error) {
        return error;
    }
}

const getUserDBById = async (userId) => {
    try {
        const params = {
            TableName: userTable,
            KeyConditionExpression: '#userId = :userId',
            ConsistentRead: true,
            ExpressionAttributeNames: {
                '#userId': 'userId'
            },
            ExpressionAttributeValues: {
                ':userId': userId
            }
        }
        const res = await docClient.query(params).promise();
        return res.Items;
    } catch (error) {
        return error;
    }
}

const getProjectDBByName = async (project) => {
    try {
        const { name, userId } = project;
        const params = {
            TableName: projectTable,
            IndexName: 'name-index',
            KeyConditionExpression: '#name = :name',
            FilterExpression: '#userId = :userId',
            ExpressionAttributeNames: {
                '#name': 'name',
                '#userId': 'userId'
            },
            ExpressionAttributeValues: {
                ':name': name,
                ':userId': userId
            }
        }
        const res = await docClient.query(params).promise();
        return res.Items;
    } catch (error) {
        return error;
    }
}

const searchTaskByNameAndProjectName = async ({ name, projectName, userId }) => {
    let params = {
        TableName: taskTable,
        KeyConditionExpression: '#name = :name',
        IndexName: 'name-createdAt-index',
        FilterExpression: '#projectName = :projectName AND #userId = :userId',
        ExpressionAttributeNames: {
            '#name': 'name',
            '#projectName': 'projectName',
            '#userId': 'userId'
        },
        ExpressionAttributeValues: {
            ':name': name,
            ':projectName': projectName,
            ':userId': userId
        }
    };

    if (!projectName) {
        delete params.ExpressionAttributeValues[':projectName'];
        params.FilterExpression = 'attribute_not_exists(projectName) AND #userId = :userId';
        delete params.ExpressionAttributeNames['#projectName']
    }
    try {
        const tasks = await docClient.query(params).promise();
        return tasks.Items[0];
    } catch (error) {
        return error
    }
};

const getTasksDB = async (userId) => {
    let tasks = [];
    let data = { LastEvaluatedKey: '1' }
    let params = {
        TableName: taskTable,
        FilterExpression: '#userId = :userId',
        ExpressionAttributeNames: {
            '#userId': 'userId'
        },
        ExpressionAttributeValues: {
            ':userId': userId
        }
    };
    while (data.LastEvaluatedKey) {
        try {
            data = await docClient.scan(params).promise();
            tasks = [...tasks, ...data.Items];
            params.ExclusiveStartKey = data.LastEvaluatedKey;
        } catch (error) {
            return error;
        }

    }
    return tasks;
}

const getProjectsDB = async (userId) => {
    let projects = [];
    let data = { LastEvaluatedKey: '1' }
    let params = {
        TableName: projectTable,
        FilterExpression: '#userId = :userId',
        ExpressionAttributeNames: {
            '#userId': 'userId'
        },
        ExpressionAttributeValues: {
            ':userId': userId
        }
    };
    while (data.LastEvaluatedKey) {
        try {
            data = await docClient.scan(params).promise();
            projects = [...projects, ...data.Items];
            params.ExclusiveStartKey = data.LastEvaluatedKey;
        } catch (error) {
            return error;
        }

    }
    return projects;
}

module.exports = {
    insertUser,
    getUserDBById,
    insertProject,
    getProjectDBByName,
    searchTaskByNameAndProjectName,
    insertTask,
    getTasksDB,
    getProjectsDB
}